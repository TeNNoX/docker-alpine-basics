# Alpine image with a few basics

[![size](https://badgen.net/docker/size/tennox/alpine-plus-basics)](https://hub.docker.com/r/tennox/alpine-plus-basics)

## Updated with latest versions [automatically every week](https://gitlab.com/TeNNoX/docker-alpine-basics/-/pipeline_schedules)

- bash *(as default shell)*
- curl
- git
- openssh-client
- rsync
- ed
- jq
- tree


```bash
docker run --rm tennox/alpine-plus-basics
# or directly from gitlab:
docker run --rm registry.gitlab.com/tennox/docker-alpine-basics
```
