FROM alpine:3.21.3

# install packages and change shell to bash (just to be safe)
RUN apk add --no-cache bash openssh-client curl git rsync ed jq tree && \
    sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd

CMD ["/bin/bash"]
